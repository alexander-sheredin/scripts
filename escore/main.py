__author__ = 'Alexander Sheredin'

import sys
from math import log

def calculate(x, y):
    print "calc: %d, %d" % (x, y)
    n_mult = n_sub = 0
    if x < y and x <= 0:
        # no solution for x < y & x <= 0
        return -1, None, None
    elif x > y and x >= 0:
        n_sub = x - y
    elif x == y:
        "no actions required, return zeros"
        pass
    elif x < y: # 0 < X < Y
        n_mult = int(log(y / x, 2)) + 1
        n_sub = x * 2 ** n_mult - y
    else: # Y < X < 0
        n_mult = int(log(y / x, 2))
        n_sub = x * 2 ** n_mult - y
    return n_mult + n_sub, n_mult, n_sub


if __name__ == '__main__':
    x = y = None
    try:
        x = int(sys.argv[1])
        y = int(sys.argv[2])
    except:
        print "No arguments provided"
        x = int(raw_input("Enter integer number X: "))
        y = int(raw_input("Enter integer number Y: "))
        pass

    n, mult, sub = calculate(x, y)
    if n >= 0:
        print "%d operations: %d times x2, %d times -1" % (n, mult, sub)
    else:
        print "Not possible (X < Y && X <= 0)\n", (n, mult, sub)


