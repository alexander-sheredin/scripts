__author__ = 'Alexander Sheredin'

import unittest
from main import calculate
from random import randint


class TestCalc(unittest.TestCase):
    def test_on_random_data(self):
        inputs = ((randint(-100, 100), randint(-100, 100)) for i in xrange(10))
        for x, y in inputs:
            n, mult, sub = calculate(x, y)
            if n >= 0:
                print "(%d, %d): X * 2 ^ %s - (1 * %s)" % (x, y, mult, sub)
                self.assertEquals(n, mult + sub)
                self.assertEquals(x * 2 ** mult - (1 * sub), y)
            else:
                print "(%d, %d): impossible" % (x, y)
                self.assertTrue(x < y and x <= 0)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCalc)
    unittest.TextTestRunner(verbosity=2).run(suite)