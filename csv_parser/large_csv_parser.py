__author__ = 'Alexander Sheredin'

import urllib2
import csv
import sys
import time

#URL = "http://incollar.kz/files/testfile158.csv"
DEFAULT_URL = "http://incollar.kz/files/testfile506.csv"
COLUMNS = ('n', 'Gender', 'Name', 'Age', 'Position', 'City', 'Token')
# Size of data to download (later)
#CHUNK = 16 * 1024


# Calculate average Women' age
def calculate_average_women_age(url=DEFAULT_URL):
    # some of women ages
    sum_age = 0
    # num of data entities
    num_age = 0
    data = urllib2.urlopen(url)
    csv_reader = csv.reader(data, delimiter=';', quotechar='|')
    for row in csv_reader:
        if row[1] == 'F':
            sum_age += int(row[3])
            num_age += 1
    if data:
        data.close()
    print "%d women found" % num_age
    return sum_age / num_age


if __name__ == '__main__':
    url = sys.argv[1] if len(sys.argv) > 1 else DEFAULT_URL
    print "Processing file: %s" % url
    time_begin = time.time()
    try:
        average = calculate_average_women_age(url)
    except Exception, e:
        print "Error: %s" % e
        sys.exit(1)
    duration = time.time() - time_begin
    print "Average women age is %d. " % average
    print "%s processing took %.5f seconds" % (url, duration)

