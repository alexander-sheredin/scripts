Write the Python script and send it to me. I'll test it and follow up with additional requests, if needed. I hope to receive the work done within few hours. The only skill needed is knowledge of Python.

Details on the job
Here's the structure of the data. I have a text file with the strings I want to match in the target data. I'm going to call this file source.txt. The file source.txt has the following columns:
Column1 - name_source - type:string
Column2 - surname_source - type:string
Column3 - id1 - type:string
Column4 - id2 - type:string
...
Column10 - id8 - type:string

I need the Python script to go row by row in the source.txt file, take name_source and surname_source, and find the following combinations

1. name_source surname_source
2. surname_source name_source

in all the rows contained in a second text file, target.txt. The structure of target.txt is as follows:
Column1 - string_target - type:string
Column2 - id_target - type:string

The string that must be searched - string_target - may contain more elements than just name_source and surname_source. So, the scripts must find substrings that match. For example, imagine that I'm trying to find a match for JOHN SMITH. The string_target "SMITH JOHN - CHICAGO, ILLINOIS" would be a match. The variable string_target might contain letters, numbers, and symbols.

When the script finds a match, I would like to add some variables to target.txt:
Column 3: matched - type:numeric - 0 for unmatched rows and 1 for matched rows
Column 4: name_source from source.txt
Column 5: surname source from source.txt
Column6 - id1 from source.txt
Column7 - id2 from source.txt
...
Column13 - id8 from source.txt

Even after finding one match, the script should keep going, finding all rows in target.txt that contains a combination of the same name_source surname_source.
Finally, and most importantly, source.txt contains around 20K rows and target.txt around 1million rows. So, I'd need the script to run as fast as possible.
