# you can use print for debugging purposes, e.g.
# print "this is a debug message"

def solution(X, A):
    # write your code in Python 2.7

    num_occ = 0
    occ = 0
    a_len = len(A)
    x_count = A.count(X)
    if x_count >= a_len:
        return -1

    try:
        occ = A.index(X, occ)
        num_occ += 1
    except:
        print "occ: %s, num_occ: %s" % (occ, num_occ)
        return -1

    while True:
        left = num_occ
        right_xcount = A[occ:].count(X)
        right = len(A[occ:]) - right_xcount
        print "occ: %s, num_occ: %s" % (occ, num_occ)
        if left == right:
            print "match"
            return occ
        elif left > right:
            print "left > right"
            return -1
        elif occ < a_len - 1:
            try:
                next_occ = A.index(X, occ+1)
            except:
                print "no more X"
                next_occ = a_len - 1

            for i in range(occ+1, next_occ + 1):
                print "i: %s, left: %s, right: %s" % (i, (A[:i], left), (A[i:], right))
                if A[i] != X:
                    if left == right:
                        print "finally match"
                        return i
                else:
                    occ = i
                    break
                right -= 1

            if next_occ == a_len - 1:
                return -1
            else:
                occ = next_occ
                num_occ += 1
        else:
            print "left < right"
            return -1

    return -1

if __name__ == "__main__":
    test_data = (5, [5, 5, 1, 7, 2, 3, 5])
    print "array: %s\n result: %s" % (test_data, solution(test_data[0], test_data[1]))
