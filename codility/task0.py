# you can use print for debugging purposes, e.g.
# print "this is a debug message"

def solution(A):
    # write your code in Python 2.7
    length = len(A)
    left_sum = [0] * length
    right_sum = [0] * length

    last = length - 1
    for i in range(length):
        if i != 0:
            left_sum[i] = left_sum[i-1] + A[i-1]
            right_sum[last - i] = A[last - i + 1] + right_sum[last - i + 1]

    for i in range(length):
        if left_sum[i] == right_sum[i]:
            return i

    return -1


if __name__ == "__main__":
    test_arr = (5, [5, 5, 1, 7, 2, 3, 5])
    print "array: %s\n eq_indexes: %s" % (test_arr, solution(test_arr))
