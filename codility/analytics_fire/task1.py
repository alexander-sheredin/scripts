__author__ = 'Alexander Sheredin'

# you can use print for debugging purposes, e.g.
# print "this is a debug message"

import random
from numpy import *

def solution(A):
    a = sorted(A)
    return min(y - x for x, y in zip(a, a[1:]))

def solution2(A):
    return min(diff(sort(A)))

def solution3(A):
    A.sort()
    return min(y - x for x, y in zip(A, A[1:]))


def solution4(A):
    length = len(A)
    A.sort()
    delta = A[1] - A[0]
    if delta == 0 or length == 2:
        return delta

    for i in range(2, length):
        delta = min((A[i] - A[i-1], delta))
        if delta == 0:
            return delta

    return delta


def solution5(A):
    length = len(A)
    A.sort()
    min_dist = A[1] - A[0]
    if not min_dist or length == 2:
        return min_dist

    for i in xrange(2, length):
        min_dist = min((A[i] - A[i-1], min_dist))
        if not min_dist:
            return min_dist

    return min_dist



if __name__ == "__main__":
    import time
    test_arr = [int(10**6*random.random()) for i in xrange(10**5)]
    start = time.time()
    result = solution(test_arr)
    end = time.time()
    print "res: %s\nexec time: %s" % (result, end - start)

    start = time.time()
    result = solution2(test_arr)
    end = time.time()
    print "res: %s\nexec time: %s" % (result, end - start)

    start = time.time()
    result = solution3(test_arr)
    end = time.time()
    print "res: %s\nexec time: %s" % (result, end - start)

    start = time.time()
    result = solution4(test_arr)
    end = time.time()
    print "res: %s\nexec time: %s" % (result, end - start)

    start = time.time()
    result = solution5(test_arr)
    end = time.time()
    print "res: %s\nexec time: %s" % (result, end - start)