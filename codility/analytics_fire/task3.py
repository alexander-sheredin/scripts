__author__ = 'Alexander Sheredin'

# you can use print for debugging purposes, e.g.
# print "this is a debug message"

import random

def solution(n):
    d = [0] * 30
    l = 0
    while (n > 0):
        d[l] = n % 2
        n //= 2
        l += 1
    #for p in xrange(1, 1 + l): THAT WAS THE BUG
    for p in xrange(1, 1 + l / 2):
        ok = True
        for i in xrange(l - p):
            if d[i] != d[i + p]:
                ok = False
                break
        if ok:
            return p
    return -1


def string_period(num):
    for i in xrange(1, len(num)/2 + 1):
        rest = num
        while rest.find(num[:i]) == 0:
            rest = rest[i:]

        if rest == "" or num[:i].find(rest) == 0:
            print num, rest
            return i
    return -1


if __name__ == "__main__":
    import time
    binumbers = ["{0:b}".format(int(10**3*random.random())) for i in xrange(10)]
    binumbers += ["100100", "1", "10", "101", "1010", "1001", "10101010111", "101011101011101011",
                  "1001001", "11", "101", "1010", "10101", "10010", "10101010111",
                  "101011101010111010", "111111111111111111111111111111",
                  "1011101110111011101110"]

    numbers = map(lambda g: int(g, 2), binumbers)
    periods = map(string_period, binumbers)

    for i in xrange(len(numbers)):
        start = time.time()
        result = solution(numbers[i])
        end = time.time()
        print (numbers[i], binumbers[i], result, periods[i])

