__author__ = 'Alexander Sheredin'

# you can use print for debugging purposes, e.g.
# print "this is a debug message"

def solution(A):
    length = len(A)

    if length == 1:
        return True

    def is_sorted(A):
        return all(A[k] <= A[k+1] for k in xrange(length-1))

    # it is not 100% clear from description if we have to check if input array
    # is already sorted
    #if is_sorted(A):
    #    return True

    for i in range(length):
        for j in range(i+1, length):
            A[i], A[j] = A[j], A[i]
            # check if it is sorted
            if is_sorted(A):
                A[i], A[j] = A[j], A[i]
                return True
            # return elements back to their places
            A[i], A[j] = A[j], A[i]

    return False


if __name__ == "__main__":
    import time
    from itertools import permutations
    arrays = []
    for i in xrange(1, 5):
        arrays += list(permutations(range(1,i+1)))

    print "arrays: ", arrays
    for arr in arrays:
        start = time.time()
        result = solution(list(arr))
        end = time.time()
        print "test_arr: %s, res: %s" % (arr, result)
